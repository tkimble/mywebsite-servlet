Deploying on app engine
-----------------------

1. Checkout the using-gappengine branch
2. Put image files into the src/main/webapp/big_static/ directory. I should move it to something better than jetty.
3. Compile by running mvn clean package
4. Upload by running mvn appengine:update