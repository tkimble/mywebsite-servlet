package peteryu.mysite;
import java.io.IOException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.ServletException;
import java.net.MalformedURLException;
import java.net.URL;

public class StaticResolverServlet extends HttpServlet {
	protected static final String staticRoot = "/__static";
	protected static final String noJsStaticRoot = "/__nojs_static";
	@Override
	protected void doGet (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		String query = request.getQueryString();
		if(query != null) {
			for (String qry : query.split("&")) {
				if (qry.equals("nojs")) {
					handleNoJS(request, response);
					return;
				}
			}
		}

		handleStandard(request, response);
	}

	protected void handleStandard (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		
		final String uri = staticRoot + request.getRequestURI();

		boolean anyHandled = //short circuiting should mean this chain will stop when one is successful
			tryFile(uri, request, response, true) ||
			tryFile(uri + ".js", request, response, false) ||
			tryFile(uri + "/main.js", request, response, false) ||
			tryFile(uri + "/index.js", request, response, false);
		if(anyHandled) return;
		
		//neither resource nor alternate resources are there
		response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
	protected void handleNoJS (HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
		if(!tryFile(noJsStaticRoot + request.getRequestURI(), request, response, true))
			response.sendError(HttpServletResponse.SC_NOT_FOUND);
	}
	/**
	 * @param uri URI of the requested object
	 * @param request The request object
	 * @param response Response object to write to, if the uri can be handled
	 * @param sendMalformedUrl Whether or not to handle malformed url exceptions. Set to true if it's the original requested URI
	 * @return Whether or not the uri was handled and the response object written to
	*/
	protected boolean tryFile(String uri, HttpServletRequest request, HttpServletResponse response, boolean sendMalformedUrl) throws ServletException, IOException{
		final URL requested;
		try{
			requested = getServletContext().getResource(uri);
		}catch(MalformedURLException e){ //malformed url
			if(sendMalformedUrl){
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);
				return true;
			}else
				return false;
		}
		if(requested != null){ //file exists in static root
			request.getRequestDispatcher(uri).forward(request, response);
			return true;
		}
		return false; //file doesn't exist there
	}
}